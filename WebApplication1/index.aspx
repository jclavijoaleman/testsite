﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication1.index1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Inicio</title>
    <link href="/Styles/jquery-ui.css" rel="stylesheet" />  
    <link href="/Styles/menu.css" rel="stylesheet" />  
    <script src="/scripts/jquery-3.3.1.min.js"></script>  
    <script src="/scripts/jquery-ui.js"></script>  
    <script src="/scripts/jquery-numeric.js"></script> 
</head>
<body>

<form id="form1" runat="server">
    <div>
            
        <ul>
            <li id="Perfil" style="float:right"><a class="active" href="/index.aspx">Bienvenido</a></li>
        </ul>
        <p></p>    
        <table>
            <tr>
                <td>
                    Cedula:
                </td>
                <td>
                    <asp:TextBox ID="txtCedula" runat="server" style="width: 200px"/>
                </td>
            </tr>
            <tr>
                <td>
                    Contraseña:
                </td>
                <td>
                    <asp:TextBox ID="txtContrasena" runat="server" style="width: 200px" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p></p>   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button class="button button2" ID="ButtonLogin" runat="server" Text="Ingresar" OnClick="ButtonLogin_Click"  />
                </td>
                <td>
                    <asp:Button class="button button1" ID="ButtonRegistro" runat="server" Text="Registrate" OnClick="ButtonRegistro_Click"  />
                </td>
            </tr>
        </table>
        
        </div>
    </form>
</body>
</html>
