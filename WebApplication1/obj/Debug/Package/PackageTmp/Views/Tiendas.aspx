﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tiendas.aspx.cs" Inherits="WebApplication1.Views.Tiendas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tareas</title>
    <link href="/Styles/jquery-ui.css" rel="stylesheet" />  
    <link href="/Styles/menu.css" rel="stylesheet" />  
    <link href="/Styles/jquery.timepicker.css" rel="stylesheet" />  
    <script src="/scripts/jquery-3.3.1.min.js"></script>  
    <script src="/scripts/jquery-ui.js"></script>
    <script>  
        $(document).ready(function () {

            $("table[id$='GridViewTiendas'] tbody tr th,td").css("padding", "3px 10px 3px 10px");

            $("#menu li a").removeClass("active");
            $("#menu li[id='Tiendas'] a").addClass("active");

            var perfil = "<%= HttpContext.Current.Session["idPerfil"] as String %>";

            if (perfil == 2) {
                $("#menu li[id='Clientes']").hide();
                $("#menu li[id='Tiendas']").hide();
            } else {
                $("#menu li[id='Mascotas']").hide();
                $("#menu li[id='Tareas']").hide();
            }

        });
    </script>  
</head>
<body>

<form id="form1" runat="server">
    <div>
            
        <ul id="menu">
            <li id="Home" ><a href="/Views/Principal.aspx">Home</a></li>
            <li id="Clientes" ><a href="/Views/Clientes.aspx">Clientes</a></li>
            <li id="Tiendas"><a href="/Views/Tiendas.aspx">Tiendas</a></li>
            <li id="Mascotas"><a href="/Views/Mascotas.aspx">Mascotas</a></li>
            <li id="Tareas"><a href="/Views/Tareas.aspx">Tareas</a></li>
            <li id="Index" style="float:right"><a href="/index.aspx">Salir</a></li>
            <li id="Perfil" style="float:right"><a class="active" href="/Views/Clientes.aspx">Perfil</a></li>              
        </ul>
    
    </div>
    <p></p>    
    <table>
        <tr>
            <td>
                Ciudad:
            </td>
            <td>
                <asp:DropDownList ID="ddlCiudad" runat="server" Width="200px" >
                    <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                </asp:DropDownList>
                <asp:HiddenField ID="txtIdTienda" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Nombre Tienda:
            </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" style="width:200px"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:Button class="button button2" ID="ButtonInsert" runat="server" Text="Add" OnClick="ButtonInsert_Click" />
                <asp:Button class="button button2" ID="ButtonUpdate" runat="server" Text="Update" OnClick="ButtonUpdate_Click" />
                <asp:Button class="button button2" ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" />
                <asp:Button class="button button1" ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
            </td>
        </tr>
    </table>
    <p></p>   
    <asp:GridView ID="GridViewTiendas" DataKeyNames="IdTienda,IdCiudad" EmptyDataText="No hay datos para mostrar." runat="server" Style="table-layout: auto" AutoGenerateColumns="false" OnSelectedIndexChanged="GridViewTiendas_SelectedIndexChanged" >
            <HeaderStyle BackColor="#0A9A9A" ForeColor="White" Font-Bold="true" Height="30"  />
            <AlternatingRowStyle BackColor="#f5f5f5" />
            <Columns>
                        
                <asp:BoundField DataField="NombreTienda" HeaderText="Nombre" SortExpression="NombreTienda" />
                <asp:BoundField DataField="NombreCiudad" HeaderText="Ciudad" SortExpression="NombreCiudad" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </form>
</body>
</html>