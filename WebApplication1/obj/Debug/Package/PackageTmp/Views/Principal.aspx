﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="WebApplication1.Views.Principal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Principal</title>
    <link href="/Styles/jquery-ui.css" rel="stylesheet" />  
    <link href="/Styles/menu.css" rel="stylesheet" />  
    <script src="/scripts/jquery-3.3.1.min.js"></script>  
    <script src="/scripts/jquery-ui.js"></script>  
    <script src="/scripts/jquery-numeric.js"></script> 
     
    <script>  
        $(document).ready(function () {

            $('#<%= txtNumero.ClientID %>').numeric({ negative: false, decimal: false });

            $("table[id$='GridViewResumen'] tbody tr th,td").css("padding", "3px 10px 3px 10px");

            $("#menu li a").removeClass("active");
            $("#menu li[id='Home'] a").addClass("active");

            var perfil = "<%= HttpContext.Current.Session["idPerfil"] as String %>";

            if (perfil == 2) {
                $("#menu li[id='Clientes']").hide();
                $("#menu li[id='Tiendas']").hide();
            } else {
                $("#menu li[id='Mascotas']").hide();
                $("#menu li[id='Tareas']").hide();
            }

        });
    </script> 
</head>
<body>

<form id="form1" runat="server">
    <div>
            
        <ul id="menu">
            <li id="Home" ><a href="/Views/Principal.aspx">Home</a></li>
            <li id="Clientes" ><a href="/Views/Clientes.aspx">Clientes</a></li>
            <li id="Tiendas"><a href="/Views/Tiendas.aspx">Tiendas</a></li>
            <li id="Mascotas"><a href="/Views/Mascotas.aspx">Mascotas</a></li>
            <li id="Tareas"><a href="/Views/Tareas.aspx">Tareas</a></li>
            <li id="Index" style="float:right"><a href="/index.aspx">Salir</a></li>
            <li id="Perfil" style="float:right"><a class="active" href="/Views/Clientes.aspx">Perfil</a></li>              
        </ul>
        <p></p>       
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblTienda" runat="server" Text="Tienda" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTienda" Visible="False" runat="server" Width="200px">
                        <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td colspan="2">
                </td>
            </tr>
            <tr>   
                <td>
                    <asp:Label ID="lblMascota" runat="server" Text="Nombre Mascota:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombre" runat="server" Width="200px" />
                </td>

                <td>
                    <asp:Label ID="lblTareas" runat="server" Text="Tareas Pendientes:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNumero" runat="server" style="width: 200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button class="button button1" ID="ButtonSearch" runat="server" Text="Buscar" OnClick="ButtonSearch_Click" />
                </td>
                <td>
                    <asp:Label ID="lblCosto" runat="server" Text="Costo Total $:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCosto" runat="server" Enabled="False" style="width: 200px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <p></p>   
        <asp:GridView ID="GridViewResumen" EmptyDataText="No hay datos para mostrar." runat="server" allowsorting="true" Style="table-layout: auto" AutoGenerateColumns="false" OnSorting="GridViewResumen_Sorting" >
            <HeaderStyle BackColor="#0A9A9A" ForeColor="White" Font-Bold="true" Height="30"  />
            <AlternatingRowStyle BackColor="#f5f5f5" />
            <Columns>                   
                <asp:BoundField DataField="NombreMascota" HeaderText="Nombre" SortExpression="NombreMascota" />
                <asp:BoundField DataField="Apodo" HeaderText="Apodo" SortExpression="Apodo" />
                <asp:BoundField DataField="costo" HeaderText="Costo Total" SortExpression="costo" />
                <asp:BoundField DataField="FechaProxima" HeaderText="Fecha Proxima Tarea" SortExpression="FechaProxima" />
                <asp:BoundField DataField="Pendientes" HeaderText="Tareas Pendientes Día" SortExpression="Pendientes" />
                <asp:BoundField DataField="Porcentaje" HeaderText="Porcentaje Tareas Realizadas" SortExpression="Porcentaje" />
                <asp:BoundField DataField="Raza" HeaderText="Raza" SortExpression="Raza" />
            </Columns>
        </asp:GridView>
        
        <asp:GridView ID="GridViewResumenAdmin" visible="False" EmptyDataText="No hay datos para mostrar." runat="server" allowsorting="true" Style="table-layout: auto" AutoGenerateColumns="false" OnSorting="GridViewResumenAdmin_Sorting" >
            <HeaderStyle BackColor="#0A9A9A" ForeColor="White" Font-Bold="true" Height="30"  />
            <AlternatingRowStyle BackColor="#f5f5f5" />
            <Columns>                   
                <asp:BoundField DataField="Cedula" HeaderText="Cedula" SortExpression="Cedula" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                <asp:BoundField DataField="NombreTienda" HeaderText="Tienda" SortExpression="NombreTienda" />
                <asp:BoundField DataField="Apodo" HeaderText="Apodo" SortExpression="Apodo" />
                <asp:BoundField DataField="NumTareas" HeaderText="Tareas Asignadas" SortExpression="NumTareas" />
            </Columns>
        </asp:GridView>

        </div>

    </form>
</body>
</html>
