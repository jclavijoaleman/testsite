﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mascotas.aspx.cs" Inherits="WebApplication1.Views.Mascotas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mascotas</title>
    <link href="/Styles/jquery-ui.css" rel="stylesheet" />  
    <link href="/Styles/menu.css" rel="stylesheet" />  
    <script src="/scripts/jquery-3.3.1.min.js"></script>  
    <script src="/scripts/jquery-ui.js"></script>  
    <script src="/scripts/jquery-numeric.js"></script> 
     
    <script>  

        $(document).ready(function () {

            $('#<%= txtFechaNacimiento.ClientID %>').datepicker(  
            {  
                dateFormat: 'dd/mm/yy',  
                changeMonth: true,  
                changeYear: true,  
                yearRange: '1950:2100'  
            });

            $("table[id$='GridViewMascotas'] tbody tr th,td").css("padding", "3px 10px 3px 10px");

            $("#menu li a").removeClass("active");
            $("#menu li[id='Mascotas'] a").addClass("active");

            var perfil = "<%= HttpContext.Current.Session["idPerfil"] as String %>";
            
            if (perfil == 2) {
                $("#menu li[id='Clientes']").hide();
                $("#menu li[id='Tiendas']").hide();
            } else {
                $("#menu li[id='Mascotas']").hide();
                $("#menu li[id='Tareas']").hide();
            }
        });
 
    </script>  
</head>
<body>

    <form id="form1" runat="server">
        <div>
            
            <ul id="menu">
                <li id="Home" ><a href="/Views/Principal.aspx">Home</a></li>
                <li id="Clientes" ><a href="/Views/Clientes.aspx">Clientes</a></li>
                <li id="Tiendas"><a href="/Views/Tiendas.aspx">Tiendas</a></li>
                <li id="Mascotas"><a href="/Views/Mascotas.aspx">Mascotas</a></li>
                <li id="Tareas"><a href="/Views/Tareas.aspx">Tareas</a></li>
                <li id="Index" style="float:right"><a href="/index.aspx">Salir</a></li>
                <li id="Perfil" style="float:right"><a class="active" href="/Views/Perfil.aspx">Perfil</a></li>              
            </ul>
            <p></p>    
            <table>
                <tr>
                    <td>
                        Apodo:
                    </td>
                    <td>
                        <asp:TextBox ID="txtApodo" runat="server" Width="200px" />
                        <asp:HiddenField ID="txtIdMascota" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Nombre:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombre" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fecha Nacimiento:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaNacimiento" runat="server" style="width:200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Sexo:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSexo" runat="server" Width="200px">
                            <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                            <asp:ListItem Value="M">Masculino</asp:ListItem>
                            <asp:ListItem Value="F">Femenino</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Raza:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRaza" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        Seleccione Foto:
                    </td>
                    <td>
                        <asp:FileUpload ID="txtFoto" accept="image/*" multiple="false" runat="server" BorderStyle="None" />
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        
                        <asp:Image ID="Image1" runat="server" />
                        
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <asp:Button class="button button2" ID="ButtonInsert" runat="server" Text="Add" OnClick="ButtonInsert_Click"/>
                        <asp:Button class="button button2" ID="ButtonUpdate" runat="server" Text="Update" OnClick="ButtonUpdate_Click" />
                        <asp:Button class="button button2" ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" />
                        <asp:Button class="button button1" ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                    </td>
                </tr>
            </table>
            <p></p>   
            <asp:GridView ID="GridViewMascotas" DataKeyNames="IdMascota" EmptyDataText="No hay datos para mostrar." runat="server" Style="table-layout: auto" AutoGenerateColumns="false" OnSelectedIndexChanged="GridViewMascotas_SelectedIndexChanged" >
                    <HeaderStyle BackColor="#0A9A9A" ForeColor="White" Font-Bold="true" Height="30"  />
                    <AlternatingRowStyle BackColor="#f5f5f5" />
                    <Columns>
                        
                        <asp:BoundField DataField="Apodo" HeaderText="Apodo" SortExpression="Apodo" />
                        <asp:BoundField DataField="NombreMascota" HeaderText="Nombre" SortExpression="NombreMascota" />
                        <asp:BoundField DataField="FechaNac" HeaderText="Fecha Nacimiento" SortExpression="FechaNac" />
                        <asp:BoundField DataField="Sexo" HeaderText="Sexo" SortExpression="Sexo" />
                        <asp:BoundField DataField="Raza" HeaderText="Raza" SortExpression="Raza" />
                        <asp:BoundField DataField="pendientes" HeaderText="Tareas Pendientes" SortExpression="pendientes" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
        </div>
    </form>
</body>
</html>
