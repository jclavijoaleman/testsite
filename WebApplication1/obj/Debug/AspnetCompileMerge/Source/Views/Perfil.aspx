﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="WebApplication1.Views.Perfil" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Clientes</title>
    <link href="/Styles/jquery-ui.css" rel="stylesheet" />  
    <link href="/Styles/menu.css" rel="stylesheet" />  
    <script src="/scripts/jquery-3.3.1.min.js"></script>  
    <script src="/scripts/jquery-ui.js"></script>  
    <script src="/scripts/jquery-numeric.js"></script> 
     
    <script>  

        $(document).ready(function () {

            $('#<%= txtFechaNacimiento.ClientID %>').datepicker(  
            {  
                dateFormat: 'dd/mm/yy',  
                changeMonth: true,  
                changeYear: true,  
                yearRange: '1950:2100'  
            });

            $('#<%= txtCedula.ClientID %>').numeric({ negative: false, decimal: false });
            $('#<%= txtTelefono.ClientID %>').numeric({ negative: false, decimal: false });

            $("#menu li a").removeClass("active");
            $("#menu li[id='Perfil'] a").addClass("active");

            var perfil = "<%= HttpContext.Current.Session["idPerfil"] as String %>";
            
            if (perfil == 2) {
                $("#menu li[id='Clientes']").hide();
                $("#menu li[id='Tiendas']").hide();
            } else {
                $("#menu li[id='Mascotas']").hide();
                $("#menu li[id='Tareas']").hide();
            }
        });
 
    </script>  
</head>
<body>

    <form id="form1" runat="server">
        <div>
            
            <ul id="menu">
                <li id="Home" ><a href="/Views/Principal.aspx">Home</a></li>
                <li id="Clientes" ><a href="/Views/Clientes.aspx">Clientes</a></li>
                <li id="Tiendas"><a href="/Views/Tiendas.aspx">Tiendas</a></li>
                <li id="Mascotas"><a href="/Views/Mascotas.aspx">Mascotas</a></li>
                <li id="Tareas"><a href="/Views/Tareas.aspx">Tareas</a></li>
                <li id="Index" style="float:right"><a href="/index.aspx">Salir</a></li>
                <li id="Perfil" style="float:right"><a class="active" href="/Views/Perfil.aspx">Perfil</a></li>              
            </ul>
            <p></p>    
            <table>
                <tr>
                    <td>
                        Cedula:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCedula" runat="server" Enabled="false" Width="200px" />
                        <asp:HiddenField ID="txtId" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Nombre:
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombre" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        Apellido:
                    </td>
                    <td>
                        <asp:TextBox ID="txtApellido" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fecha Nacimiento:
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaNacimiento" runat="server" style="width:200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Sexo:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSexo" runat="server" Width="200px">
                            <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                            <asp:ListItem Value="M">Masculino</asp:ListItem>
                            <asp:ListItem Value="F">Femenino</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Direccion:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDireccion" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Telefono:
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelefono" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" style="width: 200px"></asp:TextBox>
                    </td>
                </tr>
    
                <tr>
                    <td>
                        Ciudad:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCiudad" runat="server" AutoPostBack="True" Width="200px" OnSelectedIndexChanged="ddlCiudad_SelectedIndexChanged" >
                            <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
    
                <tr>
                    <td>
                        Tienda:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTienda" runat="server" Width="200px">
                            <asp:ListItem Value="" Selected="True">Seleccione</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td>
                        Seleccione Foto:
                    </td>
                    <td>
                        <asp:FileUpload ID="txtFoto" accept="image/*" multiple="false" runat="server" BorderStyle="None" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Label ID="lblPerfil" runat="server" Text="Label">Perfil:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPerfil" runat="server" Width="200px">
                            <asp:ListItem Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Administrador</asp:ListItem>
                            <asp:ListItem Value="2">Cliente</asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="txtIdPerfil" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Label ID="lblPassword" runat="server" Text="Label">Contraseña:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" style="width: 200px" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button class="button button2" ID="ButtonUpdate" runat="server" Text="Update" OnClick="ButtonUpdate_Click"  />
                    </td>
                    <td>
                        <asp:Button class="button button1" ID="ButtonHome" runat="server" Text="Home" OnClick="ButtonHome_Click" />
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
