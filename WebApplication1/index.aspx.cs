﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.ServiceReference1;

namespace WebApplication1
{
    public partial class index1 : System.Web.UI.Page
    {
        ServiceReference1.IUsuarios obj = new ServiceReference1.UsuariosClient();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text == "" || txtContrasena.Text == "")
            {
                Response.Write("<script>alert('Digite Cedula y Contraseña')</script>");
                return;
            }

            Usuario usuario = new Usuario();
            usuario.Cedula = Convert.ToInt64(txtCedula.Text);
            usuario.Password = txtContrasena.Text;

            Usuario user = obj.ValidarUsuario(usuario, true);

            if (user == null)
            {
                Response.Write("<script>alert('No se encontro usuario para Cedula y Contraseña ingresado, Verifique!!')</script>");
                return;
            }
            else
            {
                HttpContext.Current.Session["id"] = user.Id.ToString();
                HttpContext.Current.Session["idPerfil"] = user.IdPerfil.ToString();

                Response.Redirect(user.IdPerfil == 2 ? "Views/Perfil.aspx" : "Views/Clientes.aspx");
            }       
            
        }

        protected void ButtonRegistro_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text == "" || txtContrasena.Text == "")
            {
                Response.Write("<script>alert('Digite Cedula y Contraseña')</script>");
                return;
            }

            Usuario usuario = new Usuario();
            usuario.Cedula = Convert.ToInt64(txtCedula.Text);
            
            Usuario user = obj.ValidarUsuario(usuario, true);

            if (user != null)
            {
                Response.Write("<script>alert('Ya se encuentra Registrado')</script>");
            }
            else
            {
                usuario.Password = txtContrasena.Text;
                usuario.IdPerfil = 2;//por defecto perfil cliente

                user = obj.AddUsuario(usuario, true);

                if (user == null)
                {
                    Response.Write("<script>alert('No se pudo registrar Informacion.')</script>");
                    return;
                }

                HttpContext.Current.Session["id"] = user.Id.ToString();
                HttpContext.Current.Session["idPerfil"] = user.IdPerfil.ToString();

                Response.Redirect(user.IdPerfil == 2 ? "Views/Perfil.aspx" : "Views/Clientes.aspx");
            }
            
        }
    }
}