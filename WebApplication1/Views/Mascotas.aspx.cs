﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Mascotas : System.Web.UI.Page
    {
        ServiceReference1.IMascotas objMascota = new ServiceReference1.MascotasClient();
        ServiceReference1.IUsuarios objUsuario = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            // usuario logueado
            _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
            _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

            if (!Page.IsPostBack)
            {
                String id = (HttpContext.Current.Session["id"] as String);

                Usuario usuario = new Usuario();
                usuario.Id = Convert.ToInt32(id);

                DataSet dsUsuario = objUsuario.SearchUsuario(usuario);

                if (dsUsuario.Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                    Response.Redirect("/index.aspx");
                }
                else
                {
                    ObtenerMascotas();
                }

            }

        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtApodo.Text != "" &&
                    txtNombre.Text != "" &&
                    txtRaza.Text != "" &&
                    txtFechaNacimiento.Text != "" &&
                    ddlSexo.SelectedValue != "" &&
                    txtFoto.FileName != "")
                {
                    Mascota mascota = new Mascota();
                    
                    mascota.Nombre = txtNombre.Text;
                    mascota.Apodo = txtApodo.Text;
                    mascota.FechaNacimiento = new DateTime(Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[0]));
                    mascota.Sexo = ddlSexo.SelectedValue;
                    mascota.Raza = txtRaza.Text;
                    mascota.IdCliente = _usuarioLogin.Id;

                    string fullPath = Path.Combine(Server.MapPath("~/files"), txtFoto.FileName);
                    txtFoto.SaveAs(fullPath);

                    if (txtFoto.HasFile)
                    {
                        using (BinaryReader reader = new BinaryReader(txtFoto.PostedFile.InputStream))
                        {
                            byte[] image = reader.ReadBytes(txtFoto.PostedFile.ContentLength);
                            mascota.Foto = image;
                        }
                    }

                    Mascota mas = objMascota.AddMascota(mascota);

                    if (mas == null)
                    {
                        Response.Write("<script>alert('No se pudo insertar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerMascotas();
                        Response.Write("<script>alert('Registro Insertado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdMascota.Value != "" &&
                    txtApodo.Text != "" &&
                    txtNombre.Text != "" &&
                    txtRaza.Text != "" &&
                    txtFechaNacimiento.Text != "" &&
                    ddlSexo.SelectedValue != "")
                {
                    Mascota mascota = new Mascota();

                    mascota.Id = Convert.ToInt32(txtIdMascota.Value);
                    mascota.Nombre = txtNombre.Text;
                    mascota.Apodo = txtApodo.Text;
                    mascota.FechaNacimiento = new DateTime(Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[2]),
                        Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[1]),
                        Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[0]));
                    mascota.Sexo = ddlSexo.SelectedValue;
                    mascota.Raza = txtRaza.Text;
                    mascota.IdCliente = _usuarioLogin.Id;

                    if (txtFoto.HasFile)
                    {
                        string fullPath = Path.Combine(Server.MapPath("~/files"), txtFoto.FileName);
                        txtFoto.SaveAs(fullPath);

                        using (BinaryReader reader = new BinaryReader(txtFoto.PostedFile.InputStream))
                        {
                            byte[] image = reader.ReadBytes(txtFoto.PostedFile.ContentLength);
                            mascota.Foto = image;
                        }
                    }

                    Mascota mas = objMascota.UpdateMascota(mascota);

                    if (mas == null)
                    {
                        Response.Write("<script>alert('No se pudo Actualizar informacion!!')</script>");
                    }
                    else
                    {
                        ObtenerMascotas();
                        Response.Write("<script>alert('Registro Actualizado!!')</script>");
                    }

                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Mascota mascota = new Mascota();
                mascota.Id = Convert.ToInt32(txtIdMascota.Value);

                bool user = objMascota.DeleteMascota(mascota);
                ObtenerMascotas();
                Response.Write("<script>alert('" + (user ? "Registro Eliminado" : "No se pudo eliminar registro") + "!!')</script>");

            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ClearMascotas();
        }

        protected void GridViewMascotas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = GridViewMascotas.SelectedRow.RowIndex;
                txtIdMascota.Value = GridViewMascotas.DataKeys[rowIndex]["IdMascota"].ToString();
                txtApodo.Text = GridViewMascotas.SelectedRow.Cells[0].Text;
                txtNombre.Text = GridViewMascotas.SelectedRow.Cells[1].Text;
                txtFechaNacimiento.Text = GridViewMascotas.SelectedRow.Cells[2].Text;
                ddlSexo.SelectedValue = GridViewMascotas.SelectedRow.Cells[3].Text;
                txtRaza.Text = GridViewMascotas.SelectedRow.Cells[4].Text;

                ButtonInsert.Visible = false;
                ButtonUpdate.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = true;

                Image1.ImageUrl = "image.aspx?ImageID="+ txtIdMascota.Value;
                Image1.ResolveUrl("image.aspx?ImageID=" + txtIdMascota.Value);

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void ObtenerMascotas()
        {
            try
            {
                DataSet ds = objMascota.GetMascotas(_usuarioLogin.Id.ToString(), null);
                GridViewMascotas.DataSource = ds;
                GridViewMascotas.DataBind();

                ClearMascotas();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ClearMascotas()
        {
            txtIdMascota.Value = "";
            txtApodo.Text = "";
            txtNombre.Text = "";
            txtFechaNacimiento.Text = "";
            txtRaza.Text = "";
            ddlSexo.SelectedValue = "";
            txtFoto.Dispose();
            
            ButtonInsert.Visible = true;
            ButtonUpdate.Visible = false;
            ButtonDelete.Visible = false;
            ButtonCancel.Visible = false;

            Image1.ImageUrl = "image.aspx?ImageID=0";
            Image1.ResolveUrl("image.aspx?ImageID=0");
        }

    }

}