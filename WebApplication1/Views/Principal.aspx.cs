﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Model;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Principal : System.Web.UI.Page
    {
        ServiceReference1.IMascotas objMascota = new ServiceReference1.MascotasClient();
        ServiceReference1.IUsuarios objUsuario = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            // usuario logueado
            _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
            _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

            if (!Page.IsPostBack)
            {
                String id = (HttpContext.Current.Session["id"] as String);

                Usuario usuario = new Usuario();
                usuario.Id = Convert.ToInt32(id);

                DataSet dsUsuario = objUsuario.SearchUsuario(usuario);

                if (dsUsuario.Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                    Response.Redirect("/index.aspx");
                }
                else
                {
                    if (_usuarioLogin.IdPerfil == 1)
                    {
                        Model.Tiendas t = new Model.Tiendas();
                        ddlTienda.DataValueField = "IdTienda";
                        ddlTienda.DataTextField = "NombreTienda";
                        ddlTienda.DataSource = null;
                        ddlTienda.DataSource = t.GetTiendas("IdCIudad");
                        ddlTienda.DataBind();

                        ddlTienda.Items.Insert(0, new ListItem("Seleccione", ""));

                        ObtenerResumenAdmin();
                    }
                    else
                    {
                        ObtenerMascotas();
                    }

                }

            }

        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            if (_usuarioLogin.IdPerfil == 1)
            {
                ObtenerResumenAdmin(ddlTienda.SelectedValue);
            }
            else
            {
                ObtenerMascotas(txtNombre.Text, txtNumero.Text == "" ? 0 : Convert.ToInt32(txtNumero.Text));
            }
        }

        private void ObtenerMascotas(string nombreMascota = "", int nroTareas = 0)
        {
            try
            {
                TareasMascotas tm = new TareasMascotas();
                DataTable ds = tm.GetTareasResumen(_usuarioLogin.Id.ToString(), nombreMascota, nroTareas);
                GridViewResumen.DataSource = ds;
                GridViewResumen.DataBind();
                Session["TaskTable"] = ds;
                ClearMascotas();

                txtCosto.Text = ds.Compute("Sum(costo)", null).ToString();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ObtenerResumenAdmin(string idtienda = "")
        {
            try
            {
                TareasMascotas tm = new TareasMascotas();
                DataTable ds = tm.GetTareasResumenAdmin(idtienda);
                GridViewResumenAdmin.DataSource = ds;
                GridViewResumenAdmin.DataBind();
                Session["TaskTable"] = ds;
                ClearMascotas();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ClearMascotas()
        {
            txtNombre.Text = "";
            txtNumero.Text = "";
            txtCosto.Text = "";
            ddlTienda.SelectedValue = "";

            if (_usuarioLogin.IdPerfil == 1)
            {
                lblCosto.Visible = false;
                lblMascota.Visible = false;
                lblTareas.Visible = false;
                txtNombre.Visible = false;
                txtNumero.Visible = false;
                txtCosto.Visible = false;
                ddlTienda.Visible = true;
                lblTienda.Visible = true;

                GridViewResumenAdmin.Visible = true;
                GridViewResumen.Visible = false;
            }
            else
            {
                lblCosto.Visible = true;
                lblMascota.Visible = true;
                lblTareas.Visible = true;
                txtNombre.Visible = true;
                txtNumero.Visible = true;
                txtCosto.Visible = true;
                ddlTienda.Visible = false;
                lblTienda.Visible = false;

                GridViewResumenAdmin.Visible = false;
                GridViewResumen.Visible = true;
            }
        }

        protected void GridViewResumen_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["TaskTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                GridViewResumen.DataSource = dt;
                GridViewResumen.DataBind();
            }
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void GridViewResumenAdmin_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = Session["TaskTable"] as DataTable;

            if (dt != null)
            {

                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                GridViewResumenAdmin.DataSource = dt;
                GridViewResumenAdmin.DataBind();
            }
        }
    }
}