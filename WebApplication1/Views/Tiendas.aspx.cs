﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Model;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Tiendas : System.Web.UI.Page
    {

        ServiceReference1.IUsuarios objUsuario = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            // usuario logueado
            _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
            _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

            if (!Page.IsPostBack)
            {
                String id = (HttpContext.Current.Session["id"] as String);

                Usuario usuario = new Usuario();
                usuario.Id = Convert.ToInt32(id);

                DataSet dsUsuario = objUsuario.SearchUsuario(usuario);

                if (dsUsuario.Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                    Response.Redirect("/index.aspx");
                }
                else
                {
                    ddlCiudad.DataValueField = "IdCiudad";
                    ddlCiudad.DataTextField = "NombreCiudad";

                    Ciudades c = new Ciudades();
                    ddlCiudad.DataSource = null;
                    DataTable dtCiudades = c.GetCiudades();
                    ddlCiudad.DataSource = dtCiudades;
                    ddlCiudad.DataBind();

                    ddlCiudad.Items.Insert(0, new ListItem("Seleccione", ""));

                    ObtenerTiendas();
                }

            }
        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNombre.Text != "" &&
                    ddlCiudad.SelectedValue != "")
                {
                    Model.Tiendas t = new Model.Tiendas();
                    bool mas = t.AddTiendas(ddlCiudad.SelectedValue, txtNombre.Text);

                    if (!mas)
                    {
                        Response.Write("<script>alert('No se pudo insertar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerTiendas();
                        Response.Write("<script>alert('Registro Insertado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (txtIdTienda.Value != "")
            {
                Model.Tiendas t = new Model.Tiendas();
                bool mas = t.DeleteTiendas(txtIdTienda.Value);

                if (!mas)
                {
                    Response.Write("<script>alert('No se pudo eliminar informacion!!')</script>");
                    return;
                }
                else
                {
                    ObtenerTiendas();
                    Response.Write("<script>alert('Registro Eliminado!!')</script>");
                    return;
                }
            }
            else
            {
                Response.Write("<script>alert('Seleccione una tienda!!')</script>");
                return;
            }
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdTienda.Value != "" && 
                    txtNombre.Text != "" &&
                    ddlCiudad.SelectedValue != "")
                {
                    Model.Tiendas t = new Model.Tiendas();
                    bool mas = t.UpdateTareas(txtIdTienda.Value, ddlCiudad.SelectedValue, txtNombre.Text);

                    if (!mas)
                    {
                        Response.Write("<script>alert('No se pudo actualizar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerTiendas();
                        Response.Write("<script>alert('Registro Actualizado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ClearTiendas();
        }

        protected void GridViewTiendas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = GridViewTiendas.SelectedRow.RowIndex;
                txtIdTienda.Value = GridViewTiendas.DataKeys[rowIndex]["IdTienda"].ToString();
                txtNombre.Text = GridViewTiendas.SelectedRow.Cells[0].Text;
                ddlCiudad.SelectedValue = GridViewTiendas.DataKeys[rowIndex]["IdCiudad"].ToString();

                ButtonInsert.Visible = false;
                ButtonUpdate.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = true;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void ObtenerTiendas()
        {
            try
            {
                Model.Tiendas t = new Model.Tiendas();
                DataSet ds = t.FindTiendas();
                GridViewTiendas.DataSource = ds;
                GridViewTiendas.DataBind();

                ClearTiendas();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ClearTiendas()
        {
            txtIdTienda.Value = "";
            txtNombre.Text = "";
            ddlCiudad.SelectedValue = "";

            ButtonInsert.Visible = true;
            ButtonUpdate.Visible = false;
            ButtonDelete.Visible = false;
            ButtonCancel.Visible = false;
        }

    }
}