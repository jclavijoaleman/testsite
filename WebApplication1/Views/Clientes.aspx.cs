﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Model;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Clientes : System.Web.UI.Page
    {
        ServiceReference1.IUsuarios obj = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                // usuario logueado
                _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
                _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

                if (!Page.IsPostBack)
                {
                    String id = (HttpContext.Current.Session["id"] as String); //Request.QueryString["id"];

                    Usuario usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(id);

                    DataSet dsUsuario = obj.SearchUsuario(usuario);

                    if (dsUsuario.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                        Response.Redirect("/index.aspx");
                    }
                    else
                    {
                        DataRow drUsuario = dsUsuario.Tables[0].Rows[0];
                        LoadClientes(drUsuario);
                    }

                    
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + ex.Message + "');", true);
            }

        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCedula.Text != "" &&
                    txtNombre.Text != "" &&
                    txtApellido.Text != "" &&
                    txtDireccion.Text != "" &&
                    txtFechaNacimiento.Text != "" &&
                    txtEmail.Text != "" &&
                    ddlSexo.SelectedValue != "" &&
                    txtTelefono.Text != "" &&
                    txtPassword.Text != "" &&
                    ddlTienda.SelectedValue != "" &&
                    ddlPerfil.SelectedValue != "")
                {
                    Usuario usuariox = new Usuario();
                    usuariox.Cedula = Convert.ToInt64(txtCedula.Text);

                    usuariox = obj.ValidarUsuario(usuariox, true);

                    if (usuariox != null)
                    {
                        Response.Write("<script>alert('Existe un cliente con esta cedula.')</script>");
                        return;
                    }

                    Usuario usuario = new Usuario();
                    usuario.Cedula = Convert.ToInt64(txtCedula.Text);
                    usuario.Nombre = txtNombre.Text;
                    usuario.Apellido = txtApellido.Text;
                    usuario.Direccion = txtDireccion.Text;
                    usuario.FechaNacimiento = new DateTime(Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[0]));
                    usuario.Email = txtEmail.Text;
                    usuario.Sexo = ddlSexo.SelectedValue;
                    usuario.Telefono = Convert.ToInt64(txtTelefono.Text);
                    usuario.IdTienda = Convert.ToInt32(ddlTienda.SelectedValue);
                    usuario.IdPerfil = Convert.ToInt32(ddlPerfil.SelectedValue);
                    usuario.Password = txtPassword.Text;

                    if (txtFoto.HasFile)
                    {
                        string fullPath = Path.Combine(Server.MapPath("~/files"), txtFoto.FileName);
                        txtFoto.SaveAs(fullPath);

                        using (BinaryReader reader = new BinaryReader(txtFoto.PostedFile.InputStream))
                        {
                            byte[] image = reader.ReadBytes(txtFoto.PostedFile.ContentLength);
                            usuario.Foto = image;
                        }
                    }

                    Usuario user = obj.AddUsuario(usuario, false);

                    if (user == null)
                    {
                        Response.Write("<script>alert('No se pudo insertar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerClientes();
                        Response.Write("<script>alert('Registro Insertado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtId.Value != "" &&
                    txtCedula.Text != "" &&
                    txtNombre.Text != "" &&
                    txtApellido.Text != "" &&
                    txtDireccion.Text != "" &&
                    txtFechaNacimiento.Text != "" &&
                    txtEmail.Text != "" &&
                    ddlSexo.SelectedValue != "" &&
                    txtTelefono.Text != "" &&
                    ddlTienda.SelectedValue != "" &&
                    ddlPerfil.SelectedValue != "")
                {
                    Usuario usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(txtId.Value);
                    usuario.Cedula = Convert.ToInt64(txtCedula.Text);
                    usuario.Nombre = txtNombre.Text;
                    usuario.Apellido = txtApellido.Text;
                    usuario.Direccion = txtDireccion.Text;
                    usuario.FechaNacimiento = new DateTime(Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[0]));
                    usuario.Email = txtEmail.Text;
                    usuario.Sexo = ddlSexo.SelectedValue;
                    usuario.Telefono = Convert.ToInt64(txtTelefono.Text);
                    usuario.IdTienda = Convert.ToInt32(ddlTienda.SelectedValue);
                    usuario.IdPerfil = Convert.ToInt32(ddlPerfil.SelectedValue);

                    if (txtFoto.HasFile)
                    {
                        string fullPath = Path.Combine(Server.MapPath("~/files"), txtFoto.FileName);
                        txtFoto.SaveAs(fullPath);

                        using (BinaryReader reader = new BinaryReader(txtFoto.PostedFile.InputStream))
                        {
                            byte[] image = reader.ReadBytes(txtFoto.PostedFile.ContentLength);
                            usuario.Foto = image;
                        }
                    }

                    Usuario user = obj.UpdateUsuario(usuario);

                    if (user == null)
                    {
                        Response.Write("<script>alert('No se pudo Actualizar informacion!!')</script>");
                    }
                    else
                    {
                        ObtenerClientes();
                        Response.Write("<script>alert('Registro Actualizado!!')</script>");
                    }
                    
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }

        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario.Id = Convert.ToInt32(txtId.Value);

                bool user = obj.DeleteUsuario(usuario);
                ObtenerClientes();
                Response.Write("<script>alert('" + (user ? "Registro Eliminado" : "No se pudo eliminar registro") + "!!')</script>");

            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }

        }
        
        protected void ddlCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarTiendas();
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ClearClientes();
        }

        private void CargarTiendas()
        {
            try
            {
                Model.Tiendas t = new Model.Tiendas();
                ddlTienda.DataValueField = "IdTienda";
                ddlTienda.DataTextField = "NombreTienda";
                ddlTienda.DataSource = null;
                ddlTienda.DataSource = t.GetTiendas(ddlCiudad.SelectedValue == "" ? "-1" : ddlCiudad.SelectedValue);
                ddlTienda.DataBind();

                ddlTienda.Items.Insert(0, new ListItem("Seleccione", ""));
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        private void ObtenerClientes()
        {
            try
            {
                if (_usuarioLogin.IdPerfil == 1)
                {
                    DataSet dsUsuarios = obj.GetUsuarios();
                    GridViewClientes.DataSource = dsUsuarios;
                    GridViewClientes.DataBind();

                    ClearClientes();
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }          
        }

        private void LoadClientes(DataRow drUsuario)
        {
            ddlCiudad.DataValueField = "IdCiudad";
            ddlCiudad.DataTextField = "NombreCiudad";

            Ciudades c = new Ciudades();
            ddlCiudad.DataSource = null;
            DataTable dtCiudades = c.GetCiudades();
            ddlCiudad.DataSource = dtCiudades;
            ddlCiudad.DataBind();

            ddlCiudad.Items.Insert(0, new ListItem("Seleccione", ""));


            if (dtCiudades.Rows.Count > 0)
            {
                CargarTiendas();
            }

            if (_usuarioLogin.IdPerfil == 1)
            {
                txtCedula.Enabled = true;
                ddlPerfil.Visible = true;

                ButtonInsert.Visible = true;
                ButtonUpdate.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = false;

                ObtenerClientes();
            }
            else
            {
                txtId.Value = drUsuario["Id"].ToString();
                txtIdPerfil.Value = drUsuario["idPerfil"].ToString();
                txtCedula.Text = drUsuario["Cedula"].ToString();
                txtNombre.Text = drUsuario["Nombre"].ToString();
                txtApellido.Text = drUsuario["Apellido"].ToString();
                txtDireccion.Text = drUsuario["Direccion"].ToString();
                txtFechaNacimiento.Text = drUsuario["FechaNac"].ToString();
                txtEmail.Text = drUsuario["Email"].ToString();
                ddlSexo.SelectedValue = drUsuario["Sexo"].ToString();
                txtTelefono.Text = drUsuario["Telefono"].ToString();
                ddlCiudad.SelectedValue = drUsuario["idCiudad"].ToString();
                CargarTiendas();
                ddlTienda.SelectedValue = drUsuario["idTienda"].ToString();
                ddlPerfil.SelectedValue = drUsuario["idPerfil"].ToString();

                txtCedula.Enabled = false;
                ddlPerfil.Visible = false;

                ButtonInsert.Visible = false;
                ButtonUpdate.Visible = true;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = false;
            }
        }

        private void ClearClientes()
        {
            txtId.Value = "";
            txtCedula.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            txtFechaNacimiento.Text = "";
            txtEmail.Text = "";
            ddlSexo.SelectedValue = "";
            txtTelefono.Text = "";
            ddlTienda.SelectedValue = "";
            ddlPerfil.SelectedValue = "";
            ddlCiudad.SelectedValue = "";

            ButtonInsert.Visible = true;
            ButtonUpdate.Visible = false;
            ButtonDelete.Visible = false;
            ButtonCancel.Visible = false;

            Image1.ImageUrl = "imageCliente.aspx?ImageID=0";
            Image1.ResolveUrl("imageCliente.aspx?ImageID=0");
        }
        
        protected void GridViewClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = GridViewClientes.SelectedRow.RowIndex;
                txtId.Value = GridViewClientes.DataKeys[rowIndex]["Id"].ToString();
                txtCedula.Text = GridViewClientes.SelectedRow.Cells[0].Text;
                txtNombre.Text = GridViewClientes.SelectedRow.Cells[1].Text;
                txtApellido.Text = GridViewClientes.SelectedRow.Cells[2].Text;
                txtDireccion.Text = GridViewClientes.SelectedRow.Cells[3].Text;
                txtFechaNacimiento.Text = GridViewClientes.SelectedRow.Cells[4].Text;
                txtEmail.Text = GridViewClientes.SelectedRow.Cells[5].Text;
                ddlSexo.SelectedValue = GridViewClientes.SelectedRow.Cells[6].Text;
                txtTelefono.Text = GridViewClientes.SelectedRow.Cells[7].Text;
                ddlCiudad.SelectedValue = GridViewClientes.DataKeys[rowIndex]["IdCiudad"].ToString();
                CargarTiendas();
                ddlTienda.SelectedValue = GridViewClientes.DataKeys[rowIndex]["IdTienda"].ToString();
                ddlPerfil.SelectedValue = GridViewClientes.DataKeys[rowIndex]["IdPerfil"].ToString();

                ButtonInsert.Visible = false;
                ButtonUpdate.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = true;

                Image1.ImageUrl = "imageCliente.aspx?ImageID=" + txtId.Value;
                Image1.ResolveUrl("imageCliente.aspx?ImageID=" + txtId.Value);

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}