﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1.Views
{
    public partial class image : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ImageID"] != null)
                {
                    SqlConnection con =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                    string Query = "select Foto from Mascotas where idMascota=@idMascota";

                    SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                    sda.SelectCommand.Parameters.AddWithValue("@idMascota", Request.QueryString["ImageID"]);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        Byte[] bytes = (Byte[]) dt.Rows[0]["Foto"];
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "image/*";
                        Response.AddHeader("content-disposition", "attachment;filename=mascotas.jpg");
                        Response.BinaryWrite(bytes);
                        Response.Flush();
                        Response.End();

                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}