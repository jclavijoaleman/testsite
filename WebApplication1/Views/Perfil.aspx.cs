﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Model;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Perfil : System.Web.UI.Page
    {
        ServiceReference1.IUsuarios obj = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                // usuario logueado
                _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
                _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

                if (!Page.IsPostBack)
                {
                    String id = (HttpContext.Current.Session["id"] as String); //Request.QueryString["id"];

                    Usuario usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(id);

                    DataSet dsUsuario = obj.SearchUsuario(usuario);

                    if (dsUsuario.Tables[0].Rows.Count == 0)
                    {
                        Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                        Response.Redirect("/index.aspx");
                    }
                    else
                    {
                        DataRow drUsuario = dsUsuario.Tables[0].Rows[0];
                        LoadClientes(drUsuario);
                    }


                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + ex.Message + "');", true);
            }

        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtId.Value != "" &&
                    txtCedula.Text != "" &&
                    txtNombre.Text != "" &&
                    txtApellido.Text != "" &&
                    txtDireccion.Text != "" &&
                    txtFechaNacimiento.Text != "" &&
                    txtEmail.Text != "" &&
                    ddlSexo.SelectedValue != "" &&
                    txtTelefono.Text != "" &&
                    ddlTienda.SelectedValue != "" &&
                    ddlPerfil.SelectedValue != "")
                {
                    Usuario usuario = new Usuario();
                    usuario.Id = Convert.ToInt32(txtId.Value);
                    usuario.Cedula = Convert.ToInt64(txtCedula.Text);
                    usuario.Nombre = txtNombre.Text;
                    usuario.Apellido = txtApellido.Text;
                    usuario.Direccion = txtDireccion.Text;
                    usuario.FechaNacimiento = new DateTime(Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFechaNacimiento.Text.Split('/')[0]));
                    usuario.Email = txtEmail.Text;
                    usuario.Sexo = ddlSexo.SelectedValue;
                    usuario.Telefono = Convert.ToInt64(txtTelefono.Text);
                    usuario.IdTienda = Convert.ToInt32(ddlTienda.SelectedValue);
                    usuario.IdPerfil = Convert.ToInt32(ddlPerfil.SelectedValue);

                    if (txtFoto.HasFile)
                    {
                        string fullPath = Path.Combine(Server.MapPath("~/files"), txtFoto.FileName);
                        txtFoto.SaveAs(fullPath);

                        using (BinaryReader reader = new BinaryReader(txtFoto.PostedFile.InputStream))
                        {
                            byte[] image = reader.ReadBytes(txtFoto.PostedFile.ContentLength);
                            usuario.Foto = image;
                        }
                    }

                    Usuario user = obj.UpdateUsuario(usuario);

                    if (user == null)
                    {
                        Response.Write("<script>alert('No se pudo Actualizar informacion!!')</script>");
                    }
                    else
                    {
                        Response.Write("<script>alert('Registro Actualizado!!')</script>");
                    }

                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }

        }
        
        protected void ddlCiudad_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarTiendas();
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ClearClientes();
        }

        private void CargarTiendas()
        {
            try
            {
                Model.Tiendas t = new Model.Tiendas();
                ddlTienda.DataValueField = "IdTienda";
                ddlTienda.DataTextField = "NombreTienda";
                ddlTienda.DataSource = null;
                ddlTienda.DataSource = t.GetTiendas(ddlCiudad.SelectedValue == "" ? "-1" : ddlCiudad.SelectedValue);
                ddlTienda.DataBind();

                ddlTienda.Items.Insert(0, new ListItem("Seleccione", ""));
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }
        
        private void LoadClientes(DataRow drUsuario)
        {
            ddlCiudad.DataValueField = "IdCiudad";
            ddlCiudad.DataTextField = "NombreCiudad";

            Ciudades c = new Ciudades();
            ddlCiudad.DataSource = null;
            DataTable dtCiudades = c.GetCiudades();
            ddlCiudad.DataSource = dtCiudades;
            ddlCiudad.DataBind();

            ddlCiudad.Items.Insert(0, new ListItem("Seleccione", ""));


            if (dtCiudades.Rows.Count > 0)
            {
                CargarTiendas();
            }
            
            txtId.Value = drUsuario["Id"].ToString();
            txtIdPerfil.Value = drUsuario["idPerfil"].ToString();
            txtCedula.Text = drUsuario["Cedula"].ToString();
            txtNombre.Text = drUsuario["Nombre"].ToString();
            txtApellido.Text = drUsuario["Apellido"].ToString();
            txtDireccion.Text = drUsuario["Direccion"].ToString();
            txtFechaNacimiento.Text = drUsuario["FechaNac"].ToString();
            txtEmail.Text = drUsuario["Email"].ToString();
            ddlSexo.SelectedValue = drUsuario["Sexo"].ToString();
            txtTelefono.Text = drUsuario["Telefono"].ToString();
            ddlCiudad.SelectedValue = drUsuario["idCiudad"].ToString();
            CargarTiendas();
            ddlTienda.SelectedValue = drUsuario["idTienda"].ToString();
            ddlPerfil.SelectedValue = drUsuario["idPerfil"].ToString();

            txtCedula.Enabled = false;
            ddlPerfil.Visible = false;

        }

        private void ClearClientes()
        {
            txtId.Value = "";
            txtCedula.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            txtFechaNacimiento.Text = "";
            txtEmail.Text = "";
            ddlSexo.SelectedValue = "";
            txtTelefono.Text = "";
            ddlTienda.SelectedValue = "";
            ddlPerfil.SelectedValue = "";
            ddlCiudad.SelectedValue = "";

        }

        protected void ButtonHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Principal.aspx");
        }
    }
}