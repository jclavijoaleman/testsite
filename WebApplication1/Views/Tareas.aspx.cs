﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Model;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Views
{
    public partial class Tareas : System.Web.UI.Page
    {
        ServiceReference1.ITareas objTareas = new ServiceReference1.TareasClient();
        ServiceReference1.IUsuarios objUsuario = new ServiceReference1.UsuariosClient();
        Usuario _usuarioLogin = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            // usuario logueado
            _usuarioLogin.Id = Convert.ToInt32(HttpContext.Current.Session["id"] as String);
            _usuarioLogin.IdPerfil = Convert.ToInt32(HttpContext.Current.Session["idPerfil"] as String);

            if (!Page.IsPostBack)
            {
                String id = (HttpContext.Current.Session["id"] as String);

                Usuario usuario = new Usuario();
                usuario.Id = Convert.ToInt32(id);

                DataSet dsUsuario = objUsuario.SearchUsuario(usuario);

                if (dsUsuario.Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('No se encontro Informacion de Cliente.')</script>");
                    Response.Redirect("/index.aspx");
                }
                else
                {

                    ddlMascota.DataValueField = "IdMascota";
                    ddlMascota.DataTextField = "NombreMascota";

                    TareasMascotas t = new TareasMascotas();
                    ddlMascota.DataSource = null;
                    DataTable dt = t.GetMascotas(_usuarioLogin.Id.ToString());
                    ddlMascota.DataSource = dt;
                    ddlMascota.DataBind();
                    ddlMascota.Items.Insert(0, new ListItem("Seleccione", ""));

                    ddlTipo.DataValueField = "IdTipo";
                    ddlTipo.DataTextField = "NombreTipo";

                    ddlTipo.DataSource = null;
                    DataTable dtTipos = t.GetTipos();
                    ddlTipo.DataSource = dtTipos;
                    ddlTipo.DataBind();
                    ddlTipo.Items.Insert(0, new ListItem("Seleccione", ""));

                    ObtenerTareas();
                }

            }

        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFecha.Text != "" &&
                    txtHora.Text != "" &&
                    ddlMascota.SelectedValue != "" &&
                    ddlTarea.SelectedValue != "")
                {
                    Tarea tarea = new Tarea();

                    tarea.IdMascota = Convert.ToInt32(ddlMascota.SelectedValue);
                    tarea.IdTarea = Convert.ToInt32(ddlTarea.SelectedValue);
                    tarea.Fecha = new DateTime(Convert.ToInt32(txtFecha.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFecha.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFecha.Text.Split('/')[0]));

                    tarea.Hora = new DateTime(Convert.ToInt32(txtFecha.Text.Split('/')[2]),
                                                Convert.ToInt32(txtFecha.Text.Split('/')[1]),
                                                Convert.ToInt32(txtFecha.Text.Split('/')[0]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[0]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[1]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[2]));

                    bool mas = objTareas.AddTareas(tarea);

                    if (!mas)
                    {
                        Response.Write("<script>alert('No se pudo insertar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerTareas();
                        Response.Write("<script>alert('Registro Insertado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdTarea.Value != "" &&
                    txtFecha.Text != "" &&
                    txtHora.Text != "" &&
                    ddlMascota.SelectedValue != "" &&
                    ddlTarea.SelectedValue != "")
                {
                    Tarea tarea = new Tarea();

                    tarea.Id = Convert.ToInt32(txtIdTarea.Value);
                    tarea.IdMascota = Convert.ToInt32(ddlMascota.SelectedValue);
                    tarea.IdTarea = Convert.ToInt32(ddlTarea.SelectedValue);
                    tarea.Fecha = new DateTime(Convert.ToInt32(txtFecha.Text.Split('/')[2]),
                                                           Convert.ToInt32(txtFecha.Text.Split('/')[1]),
                                                           Convert.ToInt32(txtFecha.Text.Split('/')[0]));

                    tarea.Hora = new DateTime(Convert.ToInt32(txtFecha.Text.Split('/')[2]),
                                                Convert.ToInt32(txtFecha.Text.Split('/')[1]),
                                                Convert.ToInt32(txtFecha.Text.Split('/')[0]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[0]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[1]),
                                                Convert.ToInt32(txtHora.Text.Split(':')[2]));

                    bool mas = objTareas.UpdateTareas(tarea);

                    if (!mas)
                    {
                        Response.Write("<script>alert('No se pudo actualizar informacion!!')</script>");
                        return;
                    }
                    else
                    {
                        ObtenerTareas();
                        Response.Write("<script>alert('Registro Actualizado!!')</script>");
                        return;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Faltan campos por diligenciar!!')</script>");
                    return;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Tarea tarea = new Tarea();
                tarea.Id = Convert.ToInt32(txtIdTarea.Value);

                bool user = objTareas.DeleteTareas(tarea);
                ObtenerTareas();
                Response.Write("<script>alert('" + (user ? "Registro Eliminado" : "No se pudo eliminar registro") + "!!')</script>");

            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Cleartareas();
        }

        protected void GridViewMascotas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = GridViewMascotas.SelectedRow.RowIndex;

                txtIdTarea.Value = GridViewMascotas.DataKeys[rowIndex]["IdTareaMascota"].ToString();
                txtFecha.Text = GridViewMascotas.SelectedRow.Cells[3].Text;
                txtHora.Text = GridViewMascotas.SelectedRow.Cells[4].Text;
                ddlMascota.SelectedValue = GridViewMascotas.DataKeys[rowIndex]["IdMascota"].ToString();
                ddlTipo.SelectedValue = GridViewMascotas.DataKeys[rowIndex]["IdTipo"].ToString();
                CargarTareas();
                ddlTarea.SelectedValue = GridViewMascotas.DataKeys[rowIndex]["IdTarea"].ToString();

                ButtonInsert.Visible = false;
                ButtonUpdate.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = true;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void ObtenerTareas()
        {
            try
            {
                DataSet ds = objTareas.GetTareas(_usuarioLogin.Id.ToString());
                GridViewMascotas.DataSource = ds;
                GridViewMascotas.DataBind();

                Cleartareas();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void Cleartareas()
        {
            ddlMascota.SelectedValue = "";
            ddlTarea.SelectedValue = "";
            ddlTipo.SelectedValue = "";
            txtFecha.Text = "";
            txtHora.Text = "";

            ButtonInsert.Visible = true;
            ButtonUpdate.Visible = false;
            ButtonDelete.Visible = false;
            ButtonCancel.Visible = false;
        }

        private void CargarTareas()
        {
            try
            {
                TareasMascotas t = new TareasMascotas();
                ddlTarea.DataValueField = "IdTarea";
                ddlTarea.DataTextField = "NombreTarea";
                ddlTarea.DataSource = null;
                ddlTarea.DataSource = t.GetTiendas(ddlTipo.SelectedValue == "" ? "-1" : ddlTipo.SelectedValue);
                ddlTarea.DataBind();

                ddlTarea.Items.Insert(0, new ListItem("Seleccione", ""));
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
            }
        }

        protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarTareas();
        }
    }
}