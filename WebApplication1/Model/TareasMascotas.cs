﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class TareasMascotas
    {
        public DataTable GetTipos()
        {
            string selectQuery = @"SELECT IdTipo, NombreTipo FROM TipoTareas";

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }

            return dt;
        }

        public DataTable GetTiendas(string idTipo)
        {
            string selectQuery = @"SELECT idTarea, NombreTarea FROM Tareas WHERE idTipo = " + idTipo;

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }

            return dt;
        }

        public DataTable GetMascotas(string idcliente)
        {
            string selectQuery = @"SELECT idMascota, NombreMascota FROM Mascotas where 1=1 " + (idcliente == "" ? "" : " and Idcliente = " + idcliente);


            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }

            return dt;
        }

        public DataTable GetTareasResumen(string idcliente, string nombre = "", int num = 0)
        {
            try
            {
                DataSet ds = new DataSet();

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("prc_ObtenerResumenTareas", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@nombreMascota", nombre);
                        cmd.Parameters.AddWithValue("@numeroTareas", num);
                        cmd.Parameters.AddWithValue("@idcliente", idcliente);

                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(ds);
                        }

                    }
                }

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                return null;
            }
            
        }

        public DataTable GetTareasResumenAdmin(string nombre = "")
        {
            try
            {
                DataSet ds = new DataSet();

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("prc_ObtenerResumenAdmin", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@idTienda", nombre);

                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(ds);
                        }

                    }
                }

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                return null;
            }

        }

    }
}