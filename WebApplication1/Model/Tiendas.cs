﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class Tiendas
    {
        
        public DataTable GetTiendas(string idCiudad)
        {
            string selectQuery = @"SELECT IdTienda, NombreTienda FROM Tiendas WHERE IdCIudad = " + idCiudad;

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(selectQuery, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }

            return dt;
        }

        public bool AddTiendas(string idCiudad, string nombreTienda)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = @"INSERT INTO Tiendas (NombreTienda, IdCiudad) Values( @NombreTienda, @IdCiudad)";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@IdCiudad", idCiudad);
                cmd.Parameters.AddWithValue("@NombreTienda", nombreTienda);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataSet FindTiendas()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "select * from Tiendas t " +
                               "inner join Ciudades c on c.IdCiudad = t.IdCiudad " +
                               "Order by NombreCiudad, NombreTienda";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool DeleteTiendas(string idTienda)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = "DELETE FROM Tiendas Where IdTienda = @Id";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@Id", idTienda);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool UpdateTareas(string idTienda, string idCiudad, string nombreTienda)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string query = "UPDATE Tiendas SET  "
                               + "[IdCiudad] = @IdCiudad "
                               + ",[NombreTienda] = @NombreTienda ";

                query += " WHERE idTienda = @idTienda ";

                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@IdCiudad", idCiudad);
                cmd.Parameters.AddWithValue("@NombreTienda", nombreTienda);
                cmd.Parameters.AddWithValue("@idTienda", idTienda);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}